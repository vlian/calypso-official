import ROOT as R
from collections import namedtuple

Hist = namedtuple("Hist", "name, xtitle, ytitle, xlo, xhi, ylo, yhi, r, d, logx, logy, ndiv",
                  defaults = [None, None, None, None, None, None, 1, "hist", False, False, None])

def plot(f, name, xtitle, ytitle, xlo = None, xhi = None, ylo = None, yhi = None,
         r = 1, d = "hist", logx = False, logy = False, ndiv = None):

    h = f.Get(name)

    if xlo is not None and xhi is not None:
        h.SetAxisRange(xlo, xhi)

    if ylo is not None and yhi is not None:
        h.SetAxisRange(ylo, yhi, "Y")

    if isinstance(r, tuple):
        h.Rebin2D(r[0], r[1])        
    elif r != 1:
        h.Rebin(r)

    if xtitle is not None:
        h.GetXaxis().SetTitle(xtitle)

    if ytitle is not None:
        h.GetYaxis().SetTitle(ytitle)

    if logx:
        R.gPad.SetLogx()

    if logy:
        R.gPad.SetLogy()

    if ndiv is not None:
        h.SetNdivisions(ndiv)

    h.SetLabelSize(0.05, "X")
    h.SetTitleSize(0.05, "X")
    h.SetLabelSize(0.05, "Y")
    h.SetTitleSize(0.05, "Y")

    h.GetXaxis().SetTitleOffset(1.2)

    R.gPad.SetBottomMargin(0.15)
    R.gPad.SetLeftMargin(0.12)
    R.gPad.SetRightMargin(0.2)        

    h.Draw(d)
    return h

def plotn(f, configs, x, y, outname = "valplot"):

    c = R.TCanvas()
    c.Divide(x, y)
    c._objs = []

    if isinstance(configs, tuple):
        configs = [configs]
        
    for i, cfg in enumerate(configs):
        c.cd(i+1)
        c._objs.append(plot(f, *cfg))
        
    c.Print(f"{outname}.eps")

    return

if __name__ == "__main__":

    R.gROOT.SetBatch(True)
    R.gStyle.SetOptStat(0)

    fname = "validation.root"
    f = R.TFile.Open(fname)
    
    config = [Hist("P_d0", logy = True, xtitle = "p^{0} [GeV]", ndiv = 5, r = 5),
              Hist("Theta_d0", xtitle = "#theta [rad]", ndiv = -4),
              Hist("Mass_d0", xtitle = "m^{0} [GeV]", xlo = 0, xhi = 0.001, ndiv = 4),               
              Hist("Pt_d0", logy = True, xtitle = "p_{T}^{0} [GeV]", ndiv = 10, r = 5),
              Hist("Phi_d0", xtitle = "#phi [rad]"),
              Hist("ThetaVsP_d0", xtitle = "p^{0} [GeV]", ytitle = "#theta [rad]", logx = True, logy = True, d = "colz")
              ]

    plotn(f, config, 3, 2, "daug0")

    config = [Hist("P_d1", logy = True, xtitle = "p^{0} [GeV]", ndiv = 5, r = 5),
              Hist("Theta_d1", xtitle = "#theta [rad]", ndiv = -4),
              Hist("Mass_d1", xtitle = "m^{0} [GeV]", xlo = 0, xhi = 0.001, ndiv = 4),               
              Hist("Pt_d1", logy = True, xtitle = "p_{T}^{0} [GeV]", ndiv = 10, r = 5),
              Hist("Phi_d1", xtitle = "#phi [rad]"),
              Hist("ThetaVsP_d1", xtitle = "p^{0} [GeV]", ytitle = "#theta [rad]", logx = True, logy = True, d = "colz")
              ]

    plotn(f, config, 3, 2, "daug1")    

    config = [Hist("P_M", logy = True, xtitle = "p^{0} [GeV]", ndiv = 5, r = 5),
              Hist("Theta_M", xtitle = "#theta [rad]", ndiv = -4),
              Hist("Mass_M", xtitle = "m^{0} [GeV]", xlo = 0, xhi = 0.05),               
              Hist("Pt_M", logy = True, xtitle = "p_{T}^{0} [GeV]", ndiv = 10, r = 5),
              Hist("Phi_M", xtitle = "#phi [rad]"),
              Hist("ThetaVsP_M", xtitle = "p^{0} [GeV]", ytitle = "#theta [rad]", logx = True, logy = True, d = "colz")
              ]

    plotn(f, config, 3, 2, "mother")

    plotn(f, Hist("PIDs", xtitle="PDG Id"), 1, 1, "pid") 

    config = [Hist("ThetaVsP_M", xtitle = "p^{0} [GeV]", ytitle = "#theta [rad]", logx = True, logy = True, d = "colz"),
              Hist("ThetaVsP_d0", xtitle = "p^{0} [GeV]", ytitle = "#theta [rad]", logx = True, logy = True, d = "colz"),
              Hist("ThetaVsP_d1", xtitle = "p^{0} [GeV]", ytitle = "#theta [rad]", logx = True, logy = True, d = "colz")
              ]                   

    plotn(f, config, 2, 2, "twod")

    config = [Hist("Vtx_X", xtitle = "x [mm]", r = 5),
              Hist("Vtx_Y", xtitle = "y [mm]", r = 5),
              Hist("Vtx_Z", xtitle = "z [mm]", r = 5),
              Hist("Vtx_XY", xtitle = "x [mm]", ytitle = "y [mm]", d = "colz", r = (5,5))
              ]

    plotn(f, config, 2, 2, "vtx")
