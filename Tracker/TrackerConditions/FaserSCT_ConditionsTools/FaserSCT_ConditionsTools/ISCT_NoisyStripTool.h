/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS and FAsER collaborations
*/

#ifndef ISCT_NOISY_STRIP_TOOL
#define ISCT_NOISY_STRIP_TOOL

#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/EventContext.h"
#include <vector>


class ISCT_NoisyStripTool: virtual public IAlgTool {
public:
  virtual ~ISCT_NoisyStripTool() = default;

  DeclareInterfaceID(ISCT_NoisyStripTool, 1, 0);

  virtual std::map<std::pair<int,int>, double> getNoisyStrips(const EventContext& ctx) const = 0;
  virtual std::map<std::pair<int,int>, double> getNoisyStrips(void) const = 0;
};

#endif  // ISCT_NOISY_STRIP_TOOL
