/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS and CERN collaborations
*/

#ifndef FASERSCT_NOISY_STRIP_TOOL
#define FASERSCT_NOISY_STRIP_TOOL

#include "AthenaBaseComps/AthAlgTool.h"
#include "FaserSCT_ConditionsTools/ISCT_NoisyStripTool.h"
#include "GaudiKernel/ICondSvc.h"
#include "GaudiKernel/EventContext.h"


class FaserSCT_NoisyStripTool: public extends<AthAlgTool, ISCT_NoisyStripTool> {
public:
  FaserSCT_NoisyStripTool(const std::string& type, const std::string& name, const IInterface* parent);
  virtual ~FaserSCT_NoisyStripTool() = default;
  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override;

  virtual std::map<std::pair<int, int>, double> getNoisyStrips(const EventContext& ctx) const override;
  virtual std::map<std::pair<int, int>, double> getNoisyStrips() const override;
};

#endif  // FASERSCT_NOISY_STRIP_TOOL
