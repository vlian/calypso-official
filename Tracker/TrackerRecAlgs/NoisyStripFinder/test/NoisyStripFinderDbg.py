#!/usr/bin/env python
"""
    Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from FaserByteStreamCnvSvc.FaserByteStreamCnvSvcConfig import FaserByteStreamCnvSvcCfg
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
from NoisyStripFinder.NoisyStripFinderConfig import NoisyStripFinderCfg

log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

run = 1792
ConfigFlags.Input.Files = [f"/home/tboeckh/Documents/data/TI12/Faser-Physics-00{run}-00000.raw"]
ConfigFlags.Output.ESDFileName = f"run00{run}-00000.ESD.pool.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"
ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"
ConfigFlags.Input.ProjectName = "data21"
ConfigFlags.Input.isMC = False
ConfigFlags.GeoModel.FaserVersion = "FASER-01"
ConfigFlags.Common.isOnline = False
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.Detector.GeometryFaserSCT = True
ConfigFlags.lock()

acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolWriteCfg(ConfigFlags))
acc.merge(FaserByteStreamCnvSvcCfg(ConfigFlags))
acc.merge(FaserSCT_ClusterizationCfg(
    ConfigFlags,
    name="LevelClustering",
    DataObjectName="SCT_LEVELMODE_RDOs",
    ClusterToolTimingPattern="X1X"))
acc.merge(NoisyStripFinderCfg(ConfigFlags))

# Hack to avoid problem with our use of MC databases when isMC = False
replicaSvc = acc.getService("DBReplicaSvc")
replicaSvc.COOLSQLiteVetoPattern = ""
replicaSvc.UseCOOLSQLite = True
replicaSvc.UseCOOLFrontier = False
replicaSvc.UseGeomSQLite = True

sc = acc.run(maxEvents=-1)
sys.exit(not sc.isSuccess())
