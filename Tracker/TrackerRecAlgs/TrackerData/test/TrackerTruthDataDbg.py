#!/usr/bin/env python
""""
Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""
import sys
from AthenaCommon.Logging import log
from AthenaCommon.Constants import DEBUG
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from TrackerData.TrackerTruthDataConfig import TrackerTruthDataCfg

log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

ConfigFlags.Input.Files = ["my.HITS.pool.root"]
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"
ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"
ConfigFlags.Input.ProjectName = "data22"
ConfigFlags.Input.isMC = True
ConfigFlags.GeoModel.FaserVersion = "FASER-01"
ConfigFlags.Common.isOnline = False
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.Detector.GeometryFaserSCT = True
ConfigFlags.lock()

# Core components
acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolReadCfg(ConfigFlags))

acc.merge(TrackerTruthDataCfg(ConfigFlags))
acc.getEventAlgo("Tracker::TrackerTruthDataAlg").OutputLevel = DEBUG

# Execute and finish
sc = acc.run(maxEvents=-1)
sys.exit(not sc.isSuccess())
