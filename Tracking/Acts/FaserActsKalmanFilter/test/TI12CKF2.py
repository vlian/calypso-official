#!/usr/bin/env python

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
# from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from FaserByteStreamCnvSvc.FaserByteStreamCnvSvcConfig import FaserByteStreamCnvSvcCfg
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
from TrackerSpacePointFormation.TrackerSpacePointFormationConfig import TrackerSpacePointFinderCfg
from TrackerSegmentFit.TrackerSegmentFitConfig import SegmentFitAlgCfg
from FaserActsKalmanFilter.GhostBustersConfig import GhostBustersCfg
from FaserActsKalmanFilter.TI12CKF2Config import TI12CKF2Cfg

log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

ConfigFlags.Input.Files = ['threeStationRun6833.raw']
ConfigFlags.Output.ESDFileName = "CKF.ESD.pool.root"
ConfigFlags.addFlag("Output.xAODFileName", f"CKF.xAOD.root")
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-02"
ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"
ConfigFlags.Input.ProjectName = "data22"
ConfigFlags.Input.isMC = False
ConfigFlags.GeoModel.FaserVersion = "FASER-02"
ConfigFlags.Common.isOnline = False
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.Detector.GeometryFaserSCT = True
ConfigFlags.TrackingGeometry.MaterialSource = "Input"
ConfigFlags.lock()

acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolWriteCfg(ConfigFlags))
acc.merge(FaserByteStreamCnvSvcCfg(ConfigFlags))
acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags, DataObjectName="SCT_LEVELMODE_RDOs", ClusterToolTimingPattern="X1X"))
acc.merge(TrackerSpacePointFinderCfg(ConfigFlags))
acc.merge(SegmentFitAlgCfg(ConfigFlags, SharedHitFraction=0.61, MinClustersPerFit=5, TanThetaXZCut=0.083))
acc.merge(GhostBustersCfg(ConfigFlags))
acc.merge(TI12CKF2Cfg(ConfigFlags, noDiagnostics=False))
acc.getEventAlgo("CKF2").OutputLevel = DEBUG

# logging.getLogger('forcomps').setLevel(VERBOSE)
# acc.foreach_component("*").OutputLevel = VERBOSE
# acc.foreach_component("*ClassID*").OutputLevel = VERBOSE
# acc.getService("StoreGateSvc").Dump = True
# acc.getService("ConditionStore").Dump = True
# acc.printConfig(withDetails=True)
# ConfigFlags.dump()

# Hack to avoid problem with our use of MC databases when isMC = False
replicaSvc = acc.getService("DBReplicaSvc")
replicaSvc.COOLSQLiteVetoPattern = ""
replicaSvc.UseCOOLSQLite = True
replicaSvc.UseCOOLFrontier = False
replicaSvc.UseGeomSQLite = True

sc = acc.run(maxEvents=-1)
sys.exit(not sc.isSuccess())
